package com.jackcviers

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.Keep
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Source
import cats.effect.Async
import cats.effect.ContextShift
import cats.effect.ExitCode
import cats.effect.IO
import cats.effect.LiftIO
import cats.effect.Resource
import cats.effect.Sync
import cats.syntax.applicative._
import cats.syntax.apply._
import cats.syntax.flatMap._
import cats.syntax.functor._
import fs2.Pipe
import scala.concurrent.Await
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.concurrent.duration.Duration
import streamz.converter._

object StreamWithConversion {
  def liftFutureToF[F[_]: LiftIO: ContextShift, A](
      f: => Future[A]
  )(implicit ioCs: ContextShift[IO], ec: ExecutionContext): F[A] =
    LiftIO[F].liftIO(IO.fromFuture(IO(f)))
  def stream[F[_]: Async: LiftIO: ContextShift]: fs2.Stream[F, Nothing] = {
    val async = Async[F]
    val fContextShift: ContextShift[F] = ContextShift[F]
    (for {
      _ <- fs2.Stream.eval(async.unit)
      implicit0(ioContextShift: ContextShift[IO]) <- fs2.Stream.emit(
        IO.contextShift(ExecutionContext.Implicits.global)
      )
      implicit0(actorSystem: ActorSystem) <- fs2.Stream
        .resource[F, ActorSystem](
          Resource.make[F, ActorSystem](ActorSystem("quickStart").pure[F])(
            (s: ActorSystem) => {
              implicit val innerEc: ExecutionContext =
                ExecutionContext.Implicits.global
              liftFutureToF(s.terminate().map(_ => ()))
            }
          )
        )
      numbers = 1 to 100
      numberSourceDemonstratingRun: Source[Int, NotUsed] = Source(numbers)
      sourceEvaluatedInFs2StreamWithoutConversion = fs2.Stream.eval(
        for {
          implicit0(innerEc: ExecutionContext) <-
            ExecutionContext.Implicits.global.pure[F]
          done <- liftFutureToF(
            numberSourceDemonstratingRun
              .map(i => println(s"I am running: ${i.toString()}"))
              .run()
          )
        } yield done
      )
      convertedFromAkkaToFs2 = Source(numbers)
        .toStream(
          fContextShift,
          implicitly[Async[F]],
          implicitly,
          implicitly
        )
        .map(fs2ConvertedNumber =>
          println(s"fs2 conversion running: ${fs2ConvertedNumber.toString()}")
        )

      fs2Numbers = fs2.Stream.emits(numbers).covary[IO]
      convertedToSource = Source.fromGraph(fs2Numbers.toSource)
      fs2Sink: Pipe[IO, Int, Unit] = s => s.evalMap(i => IO(println(i)))
      akkaSink = Sink.fromGraph(fs2Sink.toSink)
      convertedDemonstratingRun = fs2.Stream
        .eval(
          for {
            implicit0(innerEc: ExecutionContext) <-
              ExecutionContext.Implicits.global.pure[F]
            done <- liftFutureToF(
              convertedToSource
                .toMat(Sink.seq)(Keep.right)
                .run()
            )
          } yield done
        )
        .flatMap { sn =>
          fs2.Stream.emits(sn)
        }
        .map((convertedToAkkaNum: Int) =>
          println(
            s"number from fs2 stream to akka stream and back: ${convertedToAkkaNum.toString()}"
          )
        )
      _ <- sourceEvaluatedInFs2StreamWithoutConversion.map(_ =>
        ()
      ) ++ convertedFromAkkaToFs2 ++ convertedDemonstratingRun
      // convertedNoneDemonstratingRun <- fs2.Stream.eval(
      //   for {
      //     implicit0(innerEc: ExecutionContext) <-
      //       ExecutionContext.Implicits.global.pure[F]
      //     done <- liftFutureToF(
      //       convertedToSource
      //         .toMat(Sink.seq)(Keep.none) // this evaluates to akka.NotUsed, and so doesn't compile
      //         .run() // this will not compile
      //     )
      //   } yield done
      // )
    } yield ExitCode.Success).drain
  }
}
