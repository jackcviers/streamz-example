name := "streamz-example"

version := "0.1-SNAPSHOT"

scalaVersion := "2.12.11"


lazy val akkaVersion = "2.6.14"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
)

resolvers += Resolver.bintrayRepo("streamz", "maven")
val streamzVersion = "0.13-RC4"

libraryDependencies ++= Seq(
  "com.github.krasserm" %% "streamz-camel-akka" % streamzVersion,
  "com.github.krasserm" %% "streamz-camel-fs2" % streamzVersion,
  "com.github.krasserm" %% "streamz-converter" % streamzVersion,
)

run / fork := true

val catsEffectVersion = "2.2.0"

libraryDependencies += "org.typelevel" %% "cats-effect" % catsEffectVersion

javacOptions += "-Xlint:deprecation"

scalacOptions += "-deprecation"
scalacOptions ++= Seq(
  "-feature",
  "-unchecked",
  "-language:postfixOps",
  "-language:higherKinds",
  "-Ypartial-unification"
)

addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1")
